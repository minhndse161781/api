const endpoint = "https://localhost:7028/api/Branches";

const getBranch = async () => {
    const response = await fetch(`${endpoint}/Get-all`);
    if (!response.ok) {
      throw new Error("Invalid Request");
    } else {
      return response.json();
    }
  };

  const addBranch = async (model) => {
    const response = await fetch(`${endpoint}/Add-branch`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: model ? JSON.stringify(model) : undefined,
    });
    if (!response.ok) {
      throw new Error("Invalid Request");
    } else {
      return response.json();
    }
  };

  const updateBranch = async (
    id,
    name,
    address,
    city,
    state,
    zipCode
  ) => {
    const model = {
      branchId: id,
      name: name,
      address: address,
      city: city,
      state: state,
      zipCode: zipCode
    };
    const response = await fetch(`${endpoint}/Update-branch`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: model ? JSON.stringify(model) : undefined,
    });
    if (!response.ok) {
      throw new Error("Invalid Request");
    } else {
      return response.json();
    }
  };

  const deleteBranch = async (id) => {
    const response = await fetch(`${endpoint}/Delete-branch?id=${id}`, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
      body: undefined,
    });
    if (!response.ok) {
      throw new Error("Invalid Request");
    } else {
      return response.json();
    }
  };

  export{
    getBranch,
    updateBranch,
    addBranch,
    deleteBranch
  }