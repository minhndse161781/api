import { getBranch, addBranch, deleteBranch, updateBranch } from "./branch.service.js";

getBranch().then(response => {
    var branches = response.data;
    var rows = '';
    branches.forEach(branch => {
        if (branch.state.trim() === "Active") {
            rows += `
                <tr data-branch-id="${branch.branchId}">
                    <td>${branch.branchId}</td>
                    <td>${branch.name.trim()}</td>
                    <td>${branch.address.trim()}</td>
                    <td>${branch.city.trim()}</td>
                    <td>${branch.state.trim()}</td>
                    <td>${branch.zipCode.trim()}</td>
                </tr>
            `;
        }
    });

    $("#branchTable tbody").html(rows);
});

