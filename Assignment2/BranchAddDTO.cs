﻿namespace CusManagement
{
    public class BranchAddDTO
    {
        public int branchId { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public string zipcode { get; set; }
    }
}
