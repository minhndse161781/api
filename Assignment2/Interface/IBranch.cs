﻿using CusManagement.Models;
namespace CusManagement.Interface
{
    public interface IBranch
    {
        Task<List<Branch>> GetList();
        Task<Branch> AddNewBranch(BranchAddDTO newBranch);
        Task<Branch> DeleteById(int branchId);
        Task<Branch> Edit(BranchDTO updateBranch);

    }
}
