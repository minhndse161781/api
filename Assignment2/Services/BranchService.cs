﻿using CusManagement.Interface;
using CusManagement.Models;
using Microsoft.EntityFrameworkCore;

namespace CusManagement.Services
{
    public class BranchService : IBranch
    {
        private readonly CusManagementContext _context;
        public BranchService(CusManagementContext context)
        {
            _context = context;
        }

        
        public async Task<List<Branch>> GetList()
        {
            try
            {
                var check = _context.Branch.ToList();
                if (check != null)
                {
                    return check;
                }
                return null;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<Branch> AddNewBranch(BranchAddDTO newBranch)
        {
            try
            {
                var branch = new Branch();
                branch.BranchId = newBranch.branchId;
                branch.Name = newBranch.name;
                branch.Address = newBranch.address;
                branch.City = newBranch.city;
                branch.State = "Active";
                branch.ZipCode = newBranch.zipcode;
       
                await this._context.Branch.AddAsync(branch);
                await this._context.SaveChangesAsync();
                return branch;

            }catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<Branch> DeleteById(int branchId)
        {
            try
            {
                var branch = await this._context.Branch.Where(x => x.BranchId.Equals(branchId)).FirstOrDefaultAsync();
                if (branch != null)
                {
                    
                    branch.State = "DeActive";
                    this._context.Branch.Update(branch);
                    await this._context.SaveChangesAsync();

                    return branch;
                }
                else
                {
                    throw new Exception("Id is not exist!!");
                }
            }catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<Branch> Edit(BranchDTO updateBranch)
        {
            try
            {
                var check = await this._context.Branch.Where(x => x.BranchId.Equals(updateBranch.branchId)).FirstOrDefaultAsync();
                if (updateBranch.name != null)
                {
                    check.Name = updateBranch.name;
                }
                if (updateBranch.address != null)  
                {
                    check.Address = updateBranch.address;
                }
                if (updateBranch.city != null)
                {
                    check.City = updateBranch.city;
                }
                if (updateBranch.zipcode != null)
                {
                    check.ZipCode = updateBranch.zipcode;
                }
                if (updateBranch.state != null)  
                {
                    check.State = updateBranch.state;
                }

                this._context.Branch.Update(check);
                await this._context.SaveChangesAsync();

                return check;

            }catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

    }
}
