﻿using CusManagement.Interface;
using CusManagement.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Reflection.Emit;

namespace CusManagement.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BranchesController : ControllerBase
    {
        private IBranch service;
        public BranchesController(IBranch service)
        {
            this.service = service;
        }

        [Route("Get-all")]
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            ResponseAPI<List<Branch>> branches = new ResponseAPI<List<Branch>>();
            try
            {
                branches.Data = await this.service.GetList();
                return Ok(branches);
            }catch (Exception ex)
            {
                branches.Message = ex.Message;
                return BadRequest(branches);
            }
        }

        [Route("Add-branch")]
        [HttpPost]
        public async Task<IActionResult> Insert(BranchAddDTO dto)
        {
            ResponseAPI<List<Branch>> branches = new ResponseAPI<List<Branch>>();
            try
            {
                branches.Data = await this.service.AddNewBranch(dto); ;
                return Ok(branches);
            }
            catch (Exception e)
            {
                branches.Message = e.Message;
                return BadRequest(branches);
            }
        }

        [Route("Delete-branch")]
        [HttpDelete]
        public async Task<IActionResult> Delete(int id)
        {
            ResponseAPI<List<Branch>> branches = new ResponseAPI<List<Branch>>();
            try
            {
                branches.Data = await this.service.DeleteById(id);
                return Ok(branches);
            }
            catch (Exception e)
            {
                branches.Message = e.Message;
                return BadRequest(branches);
            }

        }

        [Route("Update-branch")]
        [HttpPut]
        public async Task<IActionResult> Update(BranchDTO dto)
        {
            ResponseAPI<List<Branch>> branches = new ResponseAPI<List<Branch>>();
            try
            {
                branches.Data = await this.service.Edit(dto);
                return Ok(branches);
            }
            catch (Exception e)
            {
                branches.Message = e.Message;
                return BadRequest(branches);
            }

        }

    }
}
