USE [CusManagement]
GO
/****** Object:  Table [dbo].[Branch]    Script Date: 9/18/2023 9:31:50 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Branch](
	[BranchId] [int] NOT NULL,
	[Name] [nchar](10) NULL,
	[Address] [nvarchar](50) NULL,
	[City] [nvarchar](50) NULL,
	[State] [nchar](10) NULL,
	[ZipCode] [nchar](10) NULL,
 CONSTRAINT [PK_Branch] PRIMARY KEY CLUSTERED 
(
	[BranchId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Branch] ([BranchId], [Name], [Address], [City], [State], [ZipCode]) VALUES (0, N'minh      ', N'string', N'string', N'DeActive  ', N'string    ')
INSERT [dbo].[Branch] ([BranchId], [Name], [Address], [City], [State], [ZipCode]) VALUES (1, N'minh      ', N'243 Hoang Dieu', N'HCM city', N'DeActive  ', N'12000     ')
INSERT [dbo].[Branch] ([BranchId], [Name], [Address], [City], [State], [ZipCode]) VALUES (3, N'minh      ', N'243 Hoang Dieu', N'HCM city', N'Active    ', N'12000     ')
INSERT [dbo].[Branch] ([BranchId], [Name], [Address], [City], [State], [ZipCode]) VALUES (4, N'minh      ', N'243 Hoang Dieu', N'HCM city', N'Active    ', N'12000     ')
GO
